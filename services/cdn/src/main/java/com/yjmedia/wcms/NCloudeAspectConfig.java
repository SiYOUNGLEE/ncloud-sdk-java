package com.yjmedia.wcms;

import java.lang.reflect.Method;

import com.ncloud.ApiClient.ApiClientBuilder;
import com.ncloud.auth.EnvironmentCredentialsProvider;
import com.ncloud.cdn.api.V2Api;
import com.ncloud.cdn.model.RequestCdnPlusPurgeRequest;
import com.ncloud.marshaller.JsonMarshaller;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ConditionalOnProperty(prefix = "ncloud.cdn", name = "enabled", havingValue = "true")
@Aspect
@Component
public class NCloudeAspectConfig implements InitializingBean {

    private V2Api client;

    @Value("${ncloud.cdn.instance-no:}")
    private String cdnInstanceNo;

    @Value("${ncloud.cdn.credentials.accessKey:${ncloud.aws.credentials.accessKey}}")
    private String accessKey;
    @Value("${ncloud.cdn.credentials.secretKey:${ncloud.aws.credentials.secretKey}}")
    private String secretKey;


    @AfterReturning(pointcut = "execution(* com.amazonaws.services.s3.AmazonS3Client.putObject(..))", returning = "returnValue")

    public void putObject(JoinPoint joinPoint, Object returnValue) throws Throwable {
        String key = "";

        try {
            Class PutObjectRequest = Class.forName("com.amazonaws.services.s3.model.PutObjectRequest");
            if (PutObjectRequest.isInstance(joinPoint.getArgs()[0])) {
                Method method = PutObjectRequest.getMethod("getKey", null);
                key = (String)method.invoke(joinPoint.getArgs()[0], null);
            }

            if (StringUtils.isNotBlank(key)) {
                log.info("==> purge: {}", key);
                purgeRequest(key);
            }
        } catch (Exception e) {
            log.error(String.format("purge - %s", key), e);
        }
    }

    @AfterReturning(pointcut = "execution(* com.amazonaws.services.s3.AmazonS3Client.copyObject(..))")
    public void copyObject(JoinPoint joinPoint) throws Throwable {
        String key = "";

        try {
            Class CopyObjectRequest = Class.forName("com.amazonaws.services.s3.model.CopyObjectRequest");
            if (CopyObjectRequest.isInstance(joinPoint.getArgs()[0])) {
                Method method = CopyObjectRequest.getMethod("getDestinationKey", null);
                key = (String)method.invoke(joinPoint.getArgs()[0], null);
            }
            else if (4 == joinPoint.getArgs().length)
                key = (String)joinPoint.getArgs()[3];

            if (StringUtils.isNotBlank(key)) {
                log.info("==> purge: {}", key);
                purgeRequest(key);
            }
        } catch (Exception e) {
            log.error(String.format("purge - %s", key), e);
        }
    }

     public void purgeRequest(String key) {
        if (StringUtils.isBlank(key) || !key.startsWith("data2"))
            return;

        RequestCdnPlusPurgeRequest request = new RequestCdnPlusPurgeRequest();
        request.setCdnInstanceNo(cdnInstanceNo);
        request.isWholePurge(false);
        request.isWholeDomain(true);
        request.responseFormatType("JSON");
        request.addTargetFileListItem(key);

        this.client.requestCdnPlusPurgeGet(request);
    }
 
    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            this.client = new V2Api(new ApiClientBuilder()
                .addMarshaller(JsonMarshaller.getInstance())
                .setCredentialsProvider(new EnvironmentCredentialsProvider(accessKey, secretKey))
                .setConnectTimeoutMs(3000)
                .setLogging(true)
                .build());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}