package com.yjmedia.mediahub;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;


@Component
@ConditionalOnProperty(prefix = "ncloud.cdn", name = "enabled", havingValue = "true")
public class NCloudeAspectConfig extends com.yjmedia.wcms.NCloudeAspectConfig {
}