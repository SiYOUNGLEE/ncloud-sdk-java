package com.ncloud.auth;

import org.apache.commons.lang3.StringUtils;

import com.ncloud.exception.SdkException;

public class EnvironmentCredentialsProvider implements CredentialsProvider {

	public static final String ACCESS_KEY_ENV_VAR = "NCLOUD_ACCESS_KEY_ID";
	public static final String ALTERNATE_ACCESS_KEY_ENV_VAR = "NCLOUD_ACCESS_KEY";
	public static final String SECRET_KEY_ENV_VAR = "NCLOUD_SECRET_KEY";
	public static final String ALTERNATE_SECRET_KEY_ENV_VAR = "NCLOUD_SECRET_ACCESS_KEY";

	private final String accessKey;
	private final String secretKey;

	/**
	 * Instantiates a new Properties file credentials provider.
	 *
	 * @param path the path
	 */
	public EnvironmentCredentialsProvider(String accessKey, String secretKey) {
		this.accessKey = StringUtils.trim(accessKey);
		this.secretKey = StringUtils.trim(secretKey);
	}

	public Credentials getCredentials() {
		if (StringUtils.isEmpty(accessKey)
			|| StringUtils.isEmpty(secretKey)) {

			throw new SdkException(
				"Unable to load ncloud credentials from environment variables " +
					"(" + ACCESS_KEY_ENV_VAR + " (or " + ALTERNATE_ACCESS_KEY_ENV_VAR + ") and " +
					SECRET_KEY_ENV_VAR + " (or " + ALTERNATE_SECRET_KEY_ENV_VAR + "))");
		}

		return new NcloudCredentials(accessKey, secretKey);
	}

	public void refresh() {
	}
}
